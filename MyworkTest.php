<?php
require 'Mywork.php';
require_once ('vendor/phpunit/phpunit/PHPUnit/Framework/TestCase.php');
require_once ('vendor/phpunit/phpunit/PHPUnit/Framework/Assert.php');
require_once 'vendor/phpunit/phpunit/PHPUnit/Autoload.php';
 
class CalculatorTests extends PHPUnit_Framework_TestCase
{
    private $calculator;
 
    protected function setUp()
    {
        $this->calculator = new Calculator();
    }
 
    protected function tearDown()
    {
        $this->calculator = NULL;
    }
 
    public function testAdd()
    {
        $result = $this->calculator->add(1, 2);
        $this->assertEquals(3, $result);
    }
 
}